package de.pat.spring.projectPokemon.dao;

import de.pat.spring.projectPokemon.model.User;
import org.springframework.stereotype.Repository;


@Repository("userDao")
public class UserDaoImpl extends BasicDaoAbstract <User>{

    public UserDaoImpl(){

        setClazz(User.class);
    }

}
