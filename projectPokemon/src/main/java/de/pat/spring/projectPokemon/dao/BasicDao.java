package de.pat.spring.projectPokemon.dao;

import java.util.List;

/**
 * Created by schwirzke on 05.09.16.
 */
public interface BasicDao <T>{

    T findById(Object id);

    List<T> findAll();

    void create(T entity);

    void update(T entity);

    void delete(T entity);

    T getByRestriction(String restriction, Object value);


}
