package de.pat.spring.projectPokemon.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by schwirzke on 13.09.16.
 */
public class SecurityWebAppInit extends AbstractSecurityWebApplicationInitializer{
}
