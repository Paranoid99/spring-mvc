package de.pat.spring.projectPokemon.service;

import de.pat.spring.projectPokemon.model.User;

/**
 * Created by schwirzke on 15.09.16.
 */
public interface UserService {

    void update(User entity);

    User findByRestriction(String restriction, Object value);

}
