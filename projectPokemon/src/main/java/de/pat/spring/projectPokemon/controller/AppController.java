package de.pat.spring.projectPokemon.controller;


import de.pat.spring.projectPokemon.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class AppController {

	@RequestMapping(value = {"/", "/home", "/index"})
	public String homePage(ModelMap modelMap) {

		modelMap.put("greeting", "Willkommen auf meiner Seite!");
		modelMap.put("user", getUserName());
		return "welcome";
	}

	@RequestMapping(value = { "/shop"}, method = RequestMethod.GET)
	public String shopPage(ModelMap model) {
		return "shop";
	}

	@RequestMapping("/login")
	public String loginPage() {

		return "welcome";
	}

	@RequestMapping(value = "/admin")
	public String adminPage(ModelMap modelMap) {

		modelMap.put("user", getUserName());

		return "admin";
	}

	@RequestMapping(value = "/about")
	public String aboutPage(ModelMap modelMap) {

		modelMap.put("user", getUserName());

		return "about";
	}

	@RequestMapping(value = "/logout")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {

		// wir holen uns das aktuell Auth-Objekt aus dem Security-Context
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {

			// wir erzeugen einen neuen Handler, der sich für uns abmeldet
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}

		// nach Abmeldung auf der Startseite weiter
		return "redirect:/welcome?welcome";

	}


	@RequestMapping(value = "/access-denied")
	public String accessDeniedPage(ModelMap modelMap) {

		modelMap.put("user", getUserName());

		return "access-denied";
	}


	private String getUserName() {

		String result = null;

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {

			// wir casten Object -> UserDetails
			result = ((UserDetails) principal).getUsername();
		} else {
			result = principal.toString();
		}

		return result;
	}
	@RequestMapping(value ="/registerUser" )
	public  String registerUserPage(){

		return "registerUser";
	}

}