package de.pat.spring.projectPokemon.model;

/**
 * Created by schwirzke on 09.09.16.
 */
public enum State {
    ACTIVE("Active"),
    INACTIVE("Inactive"),
    DELETED("Deleted"),
    LOCKED("Locked");

    private String state;

    State(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }
}
