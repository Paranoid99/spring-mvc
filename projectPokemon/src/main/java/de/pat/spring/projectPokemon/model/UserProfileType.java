package de.pat.spring.projectPokemon.model;

import java.io.Serializable;

/**
 * Created by schwirzke on 09.09.16.
 */
public enum UserProfileType implements Serializable{

    USER("USER"),
    DBA("DBA"),
    ADMIN("ADMIN");

    String userProfileType;

    UserProfileType(String userProfileType){
        this.userProfileType = userProfileType;
    }

    public String getUserProfileType(){
        return userProfileType;
    }
}
