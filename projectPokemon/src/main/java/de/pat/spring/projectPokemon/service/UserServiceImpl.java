package de.pat.spring.projectPokemon.service;

import de.pat.spring.projectPokemon.dao.BasicDaoAbstract;
import de.pat.spring.projectPokemon.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class UserServiceImpl implements UserService{

    @Autowired
    private BasicDaoAbstract<User> userBasicDaoAbstract;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void update(User entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        userBasicDaoAbstract.update(entity);

    }

    @Override
    public User findByRestriction(String restriction, Object value) {
        return userBasicDaoAbstract.getByRestriction(restriction, value);
    }
}
