

        <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

        <h2>Formular New City</h2>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <form method="post" modelAttribute="city">
                        <div class="form-group ">

                            <label class="control-label " for="name">Name</label>
                            <input class="form-control" id="name" name="name" type="text"/>

                        </div>
                        <div class="form-group ">

                            <label class="control-label " for="select">Select a Choice</label>

                            <select class="select form-control" id="select" name="select">
                                <option value="country"

                                <c:forEach items="${countries}" var="country">
                                    <tr>
                                        <td>${country.name}</td>
                                    </tr>
                                </c:forEach>

                                </option>
                            </select>

                        </div>

                        <div class="form-group ">
                            <label class="control-label " for="district">District</label>
                            <input class="form-control" id="district" name="district" type="text"/>
                        </div>

                        <div class="form-group ">
                            <label class="control-label " for="population">Population</label>
                            <input class="form-control" id="population" name="population" type="text"/>
                        </div>

                        <div class="form-group">
                            <div>
                                <button class="btn btn-primary " name="Neue Stadt hinzufügen" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
