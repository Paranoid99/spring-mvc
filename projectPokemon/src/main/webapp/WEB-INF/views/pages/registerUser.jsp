<head>
    <!-- Website Font style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

</head>

<body>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title">Benutzerkonto erstellen</h1>
                <hr />
            </div>
        </div>
        <div class="main-login main-center">
            <form class="form-horizontal" method="post" action="#">

                <!--Vorname-->
                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Vorname</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="first_name" id="first_name"  placeholder="Bitte Vornamen eingeben"/>
                        </div>
                    </div>
                </div>
                <!--Nachname-->
                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Nachname</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="name" id="name"  placeholder="Bitte Nachnamen eingeben"/>
                        </div>
                    </div>
                </div>

                <!--Email-->
                <div class="form-group">
                    <label for="email" class="cols-sm-2 control-label">Email</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="email" id="email"  placeholder="Geben sie ihre Emailadresse ein"/>
                        </div>
                    </div>
                </div>

                <!--Username-->
                <div class="form-group">
                    <label for="username" class="cols-sm-2 control-label">Benutzername</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="username" id="username"  placeholder="Wählen sie einen Benutzernamen aus"/>
                        </div>
                    </div>
                </div>

                <!--Password-->
                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Password</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="password"  placeholder="Geben sie ihr Passwort ein"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Bestätigen sie ihr Passwort"/>
                        </div>
                    </div>
                </div>
                <!--Registrieren Button-->
                <div class="form-group ">
                    <button type="button" class="btn btn-primary btn-lg btn-block login-button">Registrieren</button>
                </div>
                <div class="login-register">
                    <a href="index.php">Login</a>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
