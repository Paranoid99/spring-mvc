<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><tiles:getAsString name="title" /></title>
	<link href="<c:url value='/static/css/main.css' />" rel="stylesheet"/>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>

</head>
 
<body class="bg-1">


	<div class="jumbotron">
		<div class="container">
			<tiles:insertAttribute name="header" />
		</div>
	</div>

<div class="mitte">

	<section id="navbar navbar-default">
		<tiles:insertAttribute name="menu" />
	</section>

	<section id="content-wrapper">
		<tiles:insertAttribute name="body" />
	</section>


	<section id="footer">
		<tiles:insertAttribute name="footer"/>
	</section>
</div>

	<script src="<c:url value='/static/js/jquery-3.1.0.min.js'/>"></script>
	<script src="<c:url value='/static/js/bootstrap.js' />"></script>
</body>
</html>